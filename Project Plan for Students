---
title: '20S ITT2 Project'
subtitle: 'Project plan, part I'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: Project plan
skip-toc: false
skip-tof: true
---

# Background

This is the semester project part 1 for ITT2 where you will work with different projects, initated by a company. This project plan will cover the project, and will match topics that are taught in parallel classes.


# Purpose

The main goal is to have a system where sensor data is sent to a cloud server, presented and collected, and data and control signal may be sent back.  
This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows:  
![project_overview](../docs/photos/project_overview_no_mesh.png "ITT2 project overview")  
Reading from the left to the right:  

* DUT: Device under test, these are the pumps from Hydac
* Hydac sensor: These are sensor of different kinds that we are to implement into the system. Initially we will work with physical sensor simulations (analog/digital).  
* Particle Boron: The embedded system to run the sensor software and to be the interface to the serial connection to the raspberry pi and the gateway to particle device cloud.  
* Computer: Hosts the particle workbench IDE (Visual studio code)  
* Raspberry Pi: A small hardened linux system hosting a dashboard and relevant programs to upload/download data from the Boron and to/from the APIs. Configuration must be reproducable with appropriate security implemented.


Project deliveries are:  

* A system reading and writing data to and from the sensors/actuators  
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.  
* Docmentation of all parts of the solution  
* Regular meeting with project stakeholders.  
* Final evaluation  

The first part of the project will be divided into 3 phases and gitlab will be used as the project management platform.


# Schedule

The project is divided into three phases from week 5 to week 15.

See the [lecture plan](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_lecture_plan.pdf) for details.


# Organization

- I believe that the Steering Committee in this project would be the teaching staff directly involved.
- The Project Manager on this project would probably be Camilla.
- The Project group would be the entirety of Group B3.
- The external resource group/persons in this project's case would be the engineers at Hydac that are giving us guidance


# Budget and resources

Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

[The risks involved in the project is evaluated, and the major issues will be listed.  
The plan will include the actions taken as part of the project design to handle the risk or the actions planned should a given risk materialize.]

- Possible points of failure, Pre Mortem: Sickness (Corona Times), Mismanaged time and/or assets, Improper communication

- Possible ways to deal with the listed points of failure, Pre Mortem:
    Sickness is something we all need to work around, especially in these Corona Times. The best way to work around this is to be vigilant in self hygiene, wiping down tables and such.
    Mismanaged Time and Assets can be minimized with better project management and by speaking with the team often to keep up to date on project deadlines and such.
    Improper Communication can be minimized by opening up multiple methods of communication, as well as checking in often to make sure that things are both going at a pace that is amicable to both sides but also so that if something comes up arrangements can be made.


# Stakeholders

[An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse interests in the project.

Possible stakeholders  
    • Internal vs. external  
    • Positive vs. negative  
    • Active vs. passive  

A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.  
This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]

Our external stakeholder in this particual project would be Hydac, who I assume is the company that we are allegedelly making sensors for. I can only assume that they are a positive external stakeholder. I assume that we are increasing the value of this project for Hydac by giving them free labour.

# Communication

[The stakeholders may have some requirements or requests as to what reports or other output is published or used. Some part of the project may be confidential.  
Whenever there is a stakeholder, there is a need for communication. It will depend on the stakeholder and their role in the project, how and how often communication is needed.  
In this section, reports, periodic emails, meetings, facebook groups and any other stakeholder communication will be described]  

- Communication with Stakeholders will most likely be dealt with primarily through email for confidential information but for reports and presentations I foresee us using shared google documents since that is what we already use those for sharing information.

# Perspectives

This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

[Evaluation is about how to gauge is the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.  
The will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.]  

We will gauge how well the project is going with weekly progress reports so we have notes to help us focus our work efforts most productively. We will of course be documenting our expenses alongside our work.

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

TBD: Students may add stuff here at their discretion

* particle.io

  Particle.io is the IoT system we use. See [https://www.particle.io/](https://www.particle.io/)

* Hydac documentation

  Hydac is supplying the hardware sensors used in the project.

  The official documentation is available at [https://www.hydac.com/de-en/service/downloads-software-on-request/documents/electronics.html](https://www.hydac.com/de-en/service/downloads-software-on-request/documents/electronics.html)


* Common project group on gitlab.com

  The project is managed using gitlab.com. The groups is at [https://gitlab.com/20s-itt-dm-project](https://gitlab.com/20s-itt-dm-project)
