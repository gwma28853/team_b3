Brief Catchup
	Henrik - Made Br0 work in networking, next step is to make switch work and deal with tcpdump. (Need to work on Documentation)
	Aleksandra - Worked on Matlab and a little on Networking. Needs help on Networking.
	Aubrey - Ahead on Embedded, behind on Networking and Programming
	Gladys - Done with MatLab, Electronics Project works now, behind on Networking
	Thobias - Worked on Matlab, Electronics. Caught up in Embedded (Documented a lot of projects)
	
Delegate Assignments
	Aubrey - Do ALL the documentation for the projects, Temperature Sensor, Email to Per (When is OLA due)
	Gladys - Duplicate Alexandra's documentation to proof it.
	Aleksandra - Calendar
	
THINGS TO CHECK OUT
	Week 40 Ex 2

THINGS TO ENDEAVOR FOR
	Embedded Assignment 3 and 4
	Google Calendar
	
GOING OVER THE TEAM CONTRACT
	Review Contract X
	Be Nice and Respect Each Other O
	Notify the team in Due Time X
	Be Willing to Share your Knowledge O
	Shared Calendar (Google Calendar)
	Agenda for meetings the day before
	
CONCERNS 
	Thobias - Wants more communication
	Henrik - Wants to work as a *group*, not as him