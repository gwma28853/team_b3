Minutes from teachers meeting
14th of September


30 - start and catch up
Everybody read the article

33 - status on last weeks projects
Problem with gitlab folder
Problem understanding the projects

36 - recap on how to move forward
Focus on IOT

38 - premortem for every project

40 - talking about this weeks assignments
“Do step one before step two”

42 - What to do now?
1 - Issues / read and test
2 - Fill out the missing pieces from last week
4 - Get started on this week - reading first
	Send first team to lab + film tutorial
5 - Skills that we individually need to work on
	ex - brush off terminal to gitlab commands
	set up RPi
