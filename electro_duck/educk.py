from gpiozero import MCP3008 #Import ADC from gpiozero
from gpiozero import LED, Button #import led and button from gpiozero
import time #imports time
import sys #import system
import thingspeak #import thingspeak lib
from w1thermsensor import W1ThermSensor #import the temperatur module

#Thingspeak
channel_id = get_key('channel.txt')  # PUT YOUR CHANNEL ID IN A FILE CALLED channel.txt LOCATED IN THE SAME FOLDER AS main.py
write_key = get_key('write_key.txt') #PUT YOUR API WRITE KEY IN A FILE CALLED wrirte_key.txt LOCATED IN THE SAME FOLDER AS main.py

# initialize the connection to thingspeak
#channel = thingspeak.Channel(id=channel_id, api_key=write_key)

#Temperatur sensor
sensor = W1ThermSensor()
temperature_in_celsius = sensor.get_temperature()

#ADC is defined here
pot = MCP3008(channel=0)

#LED
led = LED(23)
led2 = LED(24)

#Buttons
button = Button(17)
button2 = Button(27)

"""values"""
btn1_value = 0
btn2_value = 0
ADC_value = 0
avg_temp = 0


def get_key(filename, out=sys.stdout): #import functions that was included in the exsample
    out.write('using key path {}\n'.format(filename))
    with open(filename) as f:
        key = f.readline()
    out.write("- read key with {} chars\n".format(len(key.strip())))
    return key.strip()


def readBtn1(value):
    global btn1_value
    btn1_value = btn1_value +1


def readBtn2(value):
    global btn2_value
    btn2_value = btn2_value +1


def readADC(value):
    global ADC_value
    value = ADC_value


def readTemp(value):
    pass


def updateLed1():
    global ADC_value
    if ADC_value > 2:
        led.blink(0.1,0.1,3)


def updateLed2():
    led2.blink(0.5,1,1)


def thingspeakSend(btn1_value, btn2_value, ADC_value, avg_temp):
    # initialize the connection to thingspeak
    channel = thingspeak.Channel(id=channel_id, api_key=write_key)

    channel.update({1: btn1_value})
    channel.update({2: btn2_value})
    channel.update({3: ADC_value})
    channel.update({4: avg_temp})


def now_ms():
    ms = int(time.time() * 1000)
    return ms


def reset():
    global btn1_value
    global btn2_value
    global ADC_value
    global avg_temp
    btn1_value = 0
    btn2_value = 0
    ADC_value = 0
    avg_temp = 0



# initialize variables
loop_duration = 15000 # in milliseconds
loop_start = now_ms()


while True:
    if (now_ms() - loop_start > loop_duration):
        #Send values to Thingspeak here
        print("sending to Thingspeak")
        thingspeakSend(btn1_value,btn2_value, ADC_value, avg_temp)
        updateLed2()
        reset()
        loop_start = now_ms()
        continue
    else:
        #Read sensors, buttons and update led's here
        button.when_pressed = readBtn1
        button.when_pressed = readBtn2
        readADC(pot)
        readTemp(temperature_in_celsius)
        updateLed1()
        print(pot.value)
        print(f'Temperature in Celcius is: {temperature_in_celsius} degrees')
        print(f'loop elapsed ms {now_ms() - loop_start}') #print loop time